/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.kbj.solver;

import java.net.MalformedURLException;
import org.dk.genUtils;

/**
 *
 * @author root
 */
public class SolverThread extends Thread {
  private String ipMiddle;
  private SolverCalc calcPhi;
  private int Digit;
  private String CalcResult;
  
  public SolverThread(String ipMid, int ADigit){
    ipMiddle= ipMid;
    calcPhi= new SolverCalc();
    Digit= ADigit;
    CalcResult= "";
  }
  
  @Override
  public void run() {
    try {
      // compute phi part
      CalcResult= calcPhi.ComputePhiParts(Digit);
      //System.out.println("calc: " + CalcResult);
      
      // send result
      MiddleClient midc= new MiddleClient(ipMiddle);
      midc.postJobResult(Digit, CalcResult, genUtils.theID());
    } catch (MalformedURLException ex) {
      //Logger.getLogger(SolverThread.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
}
