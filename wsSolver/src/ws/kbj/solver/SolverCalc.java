/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.kbj.solver;

import java.math.*;

class dkBigInteger extends BigInteger {

  dkBigInteger(String s) {
    super(s);
  }

  dkBigInteger(int i) {
    super(String.valueOf(i));
  }

  public BigDecimal toBigDecimal() {
    return new BigDecimal(toString());
  }

  public BigInteger toBigInteger() {
    return new BigInteger(toString());
  }

  public BigDecimal divide(BigDecimal inp, MathContext mc) {
    return new BigDecimal(toString(), mc).divide(inp, mc);
  }

  public BigDecimal divideReal(dkBigInteger inp, MathContext mc) {
    return new BigDecimal(toString(), mc).divide(inp.toBigDecimal(), mc);
  }

  public BigInteger addInt(dkBigInteger inp) {
    return super.add(inp.toBigInteger());
  }
}

/**
 *
 * @author root
 */
public class SolverCalc {

  MathContext mc;

  // reuse functions type One
  private BigDecimal ComputePart_Type_1(int n, int sign, int vpow, int mulBot, int addBot) {
    // (mulBot * n) + 1
    BigDecimal vbot1 = new BigDecimal(mulBot, mc);
    BigDecimal vbot2 = new BigDecimal(addBot, mc);
    BigDecimal vbot = new BigDecimal(n, mc).multiply(vbot1, mc).add(vbot2, mc);

    // 2^vpow
    BigDecimal vtop = new BigDecimal(2, mc).pow(vpow, mc);

    // sign * 2^vpow / ((mulBot * n) + 1)
    BigDecimal ret = new BigDecimal(sign, mc).multiply(vtop, mc).divide(vbot, mc);

    return ret;
  }

  // reuse functions type Two
  private BigDecimal ComputePart_Type_2(int n, int sign, int mulBot, int addBot) {
    // (mulBot * n) + 1
    BigDecimal vbot1 = new BigDecimal(mulBot, mc);
    BigDecimal vbot2 = new BigDecimal(addBot, mc);
    BigDecimal vbot = new BigDecimal(n, mc).multiply(vbot1, mc).add(vbot2, mc);

    // sign / ((mulBot * n) + 1)
    BigDecimal ret = new BigDecimal(sign, mc).divide(vbot, mc);

    return ret;
  }

  private BigInteger pow(BigInteger base, BigInteger exponent) {
    BigInteger result = BigInteger.ONE;
    while (exponent.signum() > 0) {
      if (exponent.testBit(0)) {
        result = result.multiply(base);
      }
      base = base.multiply(base);
      exponent = exponent.shiftRight(1);
    }
    return result;
  }

  // compute Phi parts
  public String ComputePhiParts(int n) {
    int mm = 4096, nn = n > mm ? n : mm;
    mc = new MathContext(nn, RoundingMode.HALF_DOWN);

    // -1 * 2^5 / (4n + 1)
    BigDecimal p1 = ComputePart_Type_1(n, -1, 5, 4, 1);

    // -1 / (4n + 3)
    BigDecimal p2 = ComputePart_Type_2(n, -1, 4, 3);

    // 1 * 2^8 / (10n + 1)
    BigDecimal p3 = ComputePart_Type_1(n, 1, 8, 10, 1);

    // -1 * 2^6 / (10n + 3)
    BigDecimal p4 = ComputePart_Type_1(n, -1, 6, 10, 3);

    // -1 * 2^2 / (10n + 5)
    BigDecimal p5 = ComputePart_Type_1(n, -1, 2, 10, 5);

    // -1 * 2^2 / (10n + 7)
    BigDecimal p6 = ComputePart_Type_1(n, -1, 2, 10, 7);

    // 1 / (10n + 9)
    BigDecimal p7 = ComputePart_Type_2(n, 1, 10, 9);

    // add all parts
    BigDecimal parts = p1.add(p2, mc).add(p3, mc).add(p4, mc).add(p5, mc).add(p6, mc).add(p7, mc);

    // 10*n
    BigInteger bn = new BigInteger(String.valueOf(n));
    BigInteger bpow = new BigInteger("10").multiply(bn);

    // 2^(10*n)
    BigInteger base = new BigInteger(String.valueOf(2));
    base = pow(base, bpow);
    BigDecimal abot = new BigDecimal(base.toString());

    // 1 / 2^(10*n) * parts
    BigDecimal ret = new BigDecimal(-1, mc).pow(n, mc).divide(abot, mc).multiply(parts, mc);

    //return ret.toString();
    return ret.toPlainString();
  }

  public String testPhi(int n) {
    MathContext mc = new MathContext(n, RoundingMode.HALF_DOWN);

    BigDecimal front = new BigDecimal(BigInteger.ONE, mc).divide(new BigDecimal("2").pow(6));

    BigDecimal sum = new BigDecimal(BigInteger.ZERO);
    for (int i = 0; i < n; i++) {
      sum = sum.add(new BigDecimal(ComputePhiParts(i)));
    }

    sum = sum.multiply(front, mc);

    return sum.toPlainString();
  }
}
