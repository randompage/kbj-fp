/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.kbj.solver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 */
public class MiddleClient {

  private URL wsdl;
  private ws.kbj.middle.MiddleSvcService wsService;
  private ws.kbj.middle.MiddleSvc wsPort;

  public MiddleClient(String ipMid) throws MalformedURLException{
    URL awsdl= new URL("http://" + ipMid + ":9191/middle?WSDL");
    wsService = new ws.kbj.middle.MiddleSvcService(awsdl);
    wsPort = wsService.getMiddleSvcPort();
  }
  
  public String halo(java.lang.String arg0) {
    return wsPort.halo(arg0);
  }

  public String ip() throws Exception {
    return wsPort.ip();
  }

  public String serviceType() {
    return wsPort.serviceType();
  }

  
  public String RegisterSolverToMiddle(String ip, String theID) {
    return wsPort.registerSolver(ip, theID);
  }

  public String UnRegisterSolverToMiddle(String ip, String theID) {
    return wsPort.unRegisterSolver(ip, theID);
  }

  public int getRegisteredSolverCount() {
    return wsPort.getSolverCount();
  }

  
  public String RegisterClientToMiddle(String ip, String theID) {
    return wsPort.registerClient(ip, theID);
  }

  public String UnRegisterClientToMiddle(String ip, String theID) {
    return wsPort.unRegisterClient(ip, theID);
  }

  public int getRegisteredClientCount() {
    return wsPort.getClientCount();
  }
  
  public String AddRequest(int MaxDigit, long ClientID){
    return wsPort.addRequest(MaxDigit, ClientID);
  }
  
  public float getRequestProgress(long ClientID){
    return wsPort.getRequestProgress(ClientID);
  }
  
  public int getRequestNotDone(long ClientID){
    return wsPort.getRequestNotDone(ClientID);
  }
  
  public String getRequestPhi(long ClientID){
    return wsPort.getRequestLastPhi(ClientID);
  }

  public int getSingleJob(long SolverID) {
    return wsPort.getSingleJob(SolverID);
  }

  public String getMultiJobs(int JobsCount, long SolverID) {
    return wsPort.getMultiJob(JobsCount, SolverID);
  }

  public void postJobResult(int ADigit, String Result, long SolverID) {
    wsPort.postJobResult(ADigit, Result, SolverID);
  }
}
