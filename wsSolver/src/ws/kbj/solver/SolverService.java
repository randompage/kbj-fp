package ws.kbj.solver;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import ws.kbj.solver.SolverCalc;

/**
 */
@WebService
public class SolverService {

  // simple halo operation
  @WebMethod
  public String Halo(String AName) {
    return "hola " + AName;
  }

  // simple service type operation
  @WebMethod
  public String ServiceType() {
    return "solver phi";
  }

  // web method get solver ip address
  @WebMethod
  public String ip() throws SocketException {
    Enumeration<NetworkInterface> enumNI = NetworkInterface.getNetworkInterfaces();

    String ret = "";
    while (enumNI.hasMoreElements()) {
      NetworkInterface ifc = enumNI.nextElement();
      if (ifc.isUp()) {
        Enumeration<InetAddress> enumAdds = ifc.getInetAddresses();
        while (enumAdds.hasMoreElements()) {
          InetAddress addr = enumAdds.nextElement();
          if (!addr.getHostAddress().startsWith("127")){
            ret= addr.getHostAddress();
            break;
          }
        }
      }
    }

    return ret;
  }

  // compute ip parts
  @WebMethod
  public String ComputePhiSub(int n) {
    SolverCalc calc = new SolverCalc();
    return calc.ComputePhiParts(n);
  }
}
