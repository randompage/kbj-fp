/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.kbj.solver;

import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.Endpoint;
import org.dk.genUtils;
import org.dk.ipUtils;

/**
 *
 * @author root
 */
public class MiddleCheckThreadPool {

  private List<MiddleCheckThread> threads;
  private List<String> ipList;
  private int CheckCount = 0;
  private int CheckMax = 0;
  private int CurrentIdx = -1;
  private String[] ips;
  private boolean hasFound = false;
  private String ipMid;
  private boolean isSolver;

  public MiddleCheckThreadPool(String[] Addresses, boolean isASolver) {
    ipList = new ArrayList<String>();
    threads = new ArrayList<MiddleCheckThread>();
    CheckCount = 0;
    CheckMax = Addresses.length;
    ips = Addresses;
    CurrentIdx = 0;
    hasFound = false;
    isSolver = isASolver;
  }

  public void AddThread(MiddleCheckThread athread) {
    threads.add(athread);
  }

  public void TerminateThreads() throws InterruptedException {
    for (int i = 0, n = threads.size(); i < n; i++) {
      MiddleCheckThread ipc = threads.get(i);
      ipc.Terminate();
    }
  }

  private boolean isWebServiceActive_Solver() {
    boolean ret = false;
    try {
      Socket s = new Socket("0.0.0.0", 9090);
      s.close();
      ret = true;
    } catch (Exception e) {
      ret = false;
    }

    return ret;
  }

  private static void OpenWebService_Solver() throws SocketException {
    try {
      String url = "http://0.0.0.0:9090/solver";
      Endpoint.publish(url, new SolverService());
    } catch (Exception e) {
      System.err.println("Caught IOException: " + e.getMessage());
    }

    System.out.println("Solver web service ready at port 9090..");
    System.out.println(String.format("Find WSDL at http://%s:9090/solver?WSDL", ipUtils.getLocalIp()));
  }

  private boolean isWebServiceActive_Client() {
    boolean ret = false;
    try {
      Socket s = new Socket("0.0.0.0", 9292);
      s.close();
      ret = true;
    } catch (Exception e) {
      ret = false;
    }

    return ret;
  }

  private static void OpenWebService_Client() throws SocketException {
    try {
      String url = "http://0.0.0.0:9292/client";
      Endpoint.publish(url, new SolverService());
    } catch (Exception e) {
      System.err.println("Caught IOException: " + e.getMessage());
    }

    System.out.println("Client web service ready at port 9292..");
    System.out.println(String.format("Find WSDL at http://%s:9292/client?WSDL", ipUtils.getLocalIp()));
  }

  public void AddIpAddress(String ip) throws Exception {
    if (CheckWebService(ip)) {
      ipList.add(ip);
      ipMid = ip;
      System.out.println("Middle tier web service ALIVE: " + ip);

      if (isSolver) {
        // registering Solver to Middle Tier
        MiddleClient midClient = new MiddleClient(ip);
        String theID = String.valueOf(genUtils.theID());
        String ret = midClient.RegisterSolverToMiddle(ip, theID);
        System.out.println("Registering Solver to Middle Tier..");
        System.out.println(">>> MIddle Tier replied : " + ret);

        if (!isWebServiceActive_Solver()) {
          //OpenWebService_Solver();
        }

        if (isWebServiceActive_Solver()) {
          //System.out.println("Solver web service started..");
        }
      } else {//if client
        // registering Client to Middle Tier
        MiddleClient midClient = new MiddleClient(ip);
        String theID = String.valueOf(genUtils.theID());
        String ret = midClient.RegisterClientToMiddle(ip, theID);
        System.out.println("Registering Client to Middle Tier..");
        System.out.println(">>> MIddle Tier replied : " + ret);

        if (!isWebServiceActive_Client()) {
          //OpenWebService_Client();
        }

        if (isWebServiceActive_Client()) {
          //System.out.println("Client web service started..");
        }
      }

      MiddleHasFound(true);
      TerminateThreads();

    } else {
      System.out.println(ip + " is NOT valid middle tier..");
    }

    //
  }

  public void IncChecks() {
    CheckCount++;
  }

  public String getCurrentIp() {
    CurrentIdx++;
    return ips[CurrentIdx];
  }

  public boolean isAtEndOfIps() {
    return hasFound || (CurrentIdx >= ips.length);
  }

  public boolean MiddleHasFound(boolean abool) {
    hasFound = abool;
    return hasFound;
  }

  public boolean CheckWebService(String ip) throws Exception {
    boolean ret = false;

    // WSD at http://localhost:9191/middle?WSDL
    MiddleClient midClient = new MiddleClient(ip);

    // check halo
    String str = midClient.halo(ip).toLowerCase();
    if (str.startsWith("hola")) {
      ret = true;
    }

    // check service
    if (ret) {
      str = midClient.serviceType().toLowerCase().trim();
      if (str.equals("middle phi")) {
        ret = true;
      }
    }

    if (ret) {
      str = midClient.ip().toLowerCase().trim();
      ret = str.equals(ip);
    }

    return true;
  }

  public String getMiddleTierIp() {
    return ipMid;
  }

}
