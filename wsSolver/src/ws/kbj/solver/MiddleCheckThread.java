/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.kbj.solver;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dk.genUtils;
import org.dk.ipUtils;

/**
 *
 * @author root
 */
public class MiddleCheckThread extends Thread {

  private String ipAddress = "";
  private int port = 9191;
  private MiddleCheckThreadPool holder = null;
  private boolean running = false;

  public MiddleCheckThread(MiddleCheckThreadPool ahold) {
    holder = ahold;
    running = true;
  }
  
  public void Terminate(){
    running = false;
    holder.MiddleHasFound(true);
    interrupt();
  }

  @Override
  public void run() {
    while (!holder.isAtEndOfIps() && !interrupted() && running) {
      boolean ret = false;

      try {
        ipAddress = holder.getCurrentIp();
        System.out.println("Check: " + ipAddress);
        ret = InetAddress.getByName(ipAddress).isReachable(genUtils.RandomInteger(2000,3000));
      } catch (Exception ex) {
        ret = false;
        //Logger.getLogger(MiddleCheckThread.class.getName()).log(Level.SEVERE, null, ex);
      }

      if (ret) {
        try {
          Socket socket = new Socket();
          socket.connect(new InetSocketAddress(ipAddress, port), genUtils.RandomInteger(2000,3000));
          socket.close();
          ret = true;
        } catch (Exception ex) {
          //Logger.getLogger(MiddleCheckThread.class.getName()).log(Level.SEVERE, null, ex);
          ret = false;
        }
      }

      if (ret) {
        System.out.println(ipAddress + " >> host alive + port opened..");
        try {
          holder.AddIpAddress(ipAddress);
        } catch (Exception ex) {
          Logger.getLogger(MiddleCheckThread.class.getName()).log(Level.SEVERE, null, ex);
        }
      }

      holder.IncChecks();
      if (ret) {
        holder.MiddleHasFound(true);
        Terminate();
        break;
      }
    }
  }
}
