package ws.kbj.solver;

import org.dk.*;

/**
 */
public class SolverMain {

  private static MiddleCheckThreadPool MiddleCheckPool;

  private static void SearchMiddleTier() throws Exception {
    int numThreads = 50;

    String[] addresses = ipUtils.getIpAddressInSubnet();
    MiddleCheckPool = new MiddleCheckThreadPool(addresses, true);
    MiddleCheckThread[] threads = new MiddleCheckThread[numThreads];

    for (int i = 0; i < numThreads; i++) {
      threads[i] = new MiddleCheckThread(MiddleCheckPool);
      threads[i].start();
      MiddleCheckPool.AddThread(threads[i]);
    }

    for (int i = 0; i < numThreads; i++) {
      threads[i].join();
    }
  }

  private static void StartActions() throws Exception {
    String ipMid = MiddleCheckPool.getMiddleTierIp();
    if (ipMid.trim().length() < 1) {
      return;
    }

    MiddleClient midc = new MiddleClient(ipMid);
    //dummy
    //midc.AddRequest(genUtils.RandomInteger(200, 300), genUtils.theID());

    int blankJobs = 0;
    while (blankJobs < 10000) {
      //System.out.println("blank " + blankJobs);

      int jobCount = 10;
      String jobMulti = midc.getMultiJobs(jobCount, genUtils.theID());
      jobMulti = jobMulti.replaceAll("(?i),null", "").replaceAll("(?i)null", "");
      if (jobMulti.length() < 1) {
        int waitTime = 1000 + 100 * blankJobs;
        waitTime = waitTime > 10000 ? 10000 : waitTime;
        System.out.println("..idle.. wait= " + (float) waitTime / 1000 + "s");
        Thread.sleep(waitTime);
        blankJobs++;
        continue;
      }

      // reset blankJobs
      blankJobs = 0;
      System.out.println("Jobs Multi: " + jobMulti);

      String[] jobs = jobMulti.split(",");
      SolverThread[] threads = new SolverThread[jobCount];

      int jobExec = 0;
      for (int i = 0, n = jobs.length; i < n; i++) {
        String ajob = jobs[i].trim().toLowerCase();
        if (ajob.equals("null") || ajob.length() < 1) {
          continue;
        }

        int digit = Integer.parseInt(ajob);
        //System.out.println("SolverThread: " + digit);

        threads[i] = new SolverThread(ipMid, digit);
        threads[i].start();

        jobExec++;
      }

      for (int i = 0, n = jobs.length; i < n; i++) {
        if (threads[i] != null) {
          threads[i].join();
        }
      }

      Thread.sleep(500);
      if (jobExec < jobCount) {
        blankJobs++;
      }
    }
  }

  /**
   */
  public static void main(String[] args) throws Exception {
    //SolverCalc a = new SolverCalc();
    //System.out.println(a.testPhi(1000)); 

    SearchMiddleTier();

    // safe check MiddleTier ip
    try {
      String ipMid = MiddleCheckPool.getMiddleTierIp();
      System.out.println(String.format("\nMiddle Tier: %s \n", ipMid));

      // start solving
      StartActions();
    } catch (Exception e) {
      System.out.println("Middle Tier ip address not found.. \n\n");
    }
  }
}
