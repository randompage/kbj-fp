/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.kbj.client;

import org.dk.genUtils;
import org.dk.ipUtils;
import ws.kbj.solver.MiddleCheckThread;
import ws.kbj.solver.MiddleCheckThreadPool;
import ws.kbj.solver.MiddleClient;

/**
 *
 * @author root
 */
public class ClientMain {

  private static MiddleCheckThreadPool MiddleCheckPool;

  private static void SearchMiddleTier() throws Exception {
    String[] addresses = ipUtils.getIpAddressInSubnet();
    MiddleCheckPool = new MiddleCheckThreadPool(addresses, false);
    MiddleCheckThread[] threads = new MiddleCheckThread[10];

    for (int i = 0; i < 10; i++) {
      threads[i] = new MiddleCheckThread(MiddleCheckPool);
      threads[i].start();
      MiddleCheckPool.AddThread(threads[i]);
    }

    for (int i = 0; i < 10; i++) {
      threads[i].join();
    }
  }

  private static void StartActions(int Digits) throws Exception {
    String ipMid = MiddleCheckPool.getMiddleTierIp();
    if (ipMid.trim().length() < 1) {
      return;
    }

    MiddleClient midc = new MiddleClient(ipMid);

    // random request if not defined
    int randReq = Digits>0? Digits : genUtils.RandomInteger(400, 500);
    midc.AddRequest(randReq, genUtils.theID());
    System.out.println("Send random request, digit=" + randReq);

    int notDone = 1, lastNotDone = 0, sameNotDone = 0;
    double progress = 0;

    while (notDone > 0) {
      lastNotDone = notDone;
      notDone = midc.getRequestNotDone(genUtils.theID());
      if (lastNotDone == notDone) {
        sameNotDone++;
      }
      if (sameNotDone > 10) {
        break;
      }
      if (notDone < 1) {
        break;
      }

      progress = (float) (randReq - notDone) / randReq * 100;
      System.out.format("NotDone= %6d, Progress= %6.2f %%\n", notDone, progress);

      Thread.sleep(3000); //sleep 3 seconds
    }

    if (notDone < 1) {
      String phi = midc.getRequestPhi(genUtils.theID());
      System.out.printf("\n\n%s completed, length=%d, PHI= %s \n\n", "100%", phi.length(), phi);
    } else {

    }
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) throws Exception {
    int Digits=0;
    if (args.length>0 && args[0].length()>0){
      Digits= Integer.parseInt(args[0]);
    }
    SearchMiddleTier();
    StartActions(Digits);
  }

}
