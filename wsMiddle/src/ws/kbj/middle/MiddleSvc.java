package ws.kbj.middle;

import java.net.SocketException;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import org.dk.ipUtils;

/**
 */
@WebService
@SOAPBinding(style = Style.RPC)
public class MiddleSvc {

  private PhiAllRequests AllReqs;
  private HostList AllHosts;

  MiddleSvc(PhiAllRequests theAllReqs, HostList theHosts) {
    AllReqs = theAllReqs;
    AllHosts = theHosts;
  }

  // simple halo operation
  @WebMethod
  public String Halo(String AName) {
    return "hola " + AName;
  }

  // simple service type operation
  @WebMethod
  public String ServiceType() {
    return "middle phi";
  }

  // web method get ip address
  @WebMethod
  public String ip() throws SocketException {
    return ipUtils.getLocalIp();
  }

  // registering solver ip address
  @WebMethod
  public String RegisterSolver(String ip, String theID) {
    int count = AllHosts.RegisterSolver(ip, theID);
    String ret = String.format("Register Solver: ip=%s, ID=%s, Total=%d", ip, theID, count);
    System.out.println(ret);
    return ret;
  }

  // un-registering solver ip address
  @WebMethod
  public String UnRegisterSolver(String ip, String theID) {
    int count = AllHosts.unRegisterSolver(ip, theID);
    String ret = String.format("unRegister Solver: ip=%s, ID=%s, Total=%d", ip, theID, count);
    System.out.println(ret);
    return ret;
  }

  // get solver count
  @WebMethod
  public int getSolverCount() {
    int count = AllHosts.getSolverCount();
    String ret = String.format("Solver Count: %d", count);
    System.out.println(ret);
    return count;
  }

  // registering client ip address
  @WebMethod
  public String RegisterClient(String ip, String theID) {
    int count = AllHosts.RegisterClient(ip, theID);
    String ret = String.format("Register Client: ip=%s, ID=%s, Total=%d", ip, theID, count);
    System.out.println(ret);
    return ret;
  }

  // un-registering client ip address
  @WebMethod
  public String UnRegisterClient(String ip, String theID) {
    int count = AllHosts.unRegisterClient(ip, theID);
    String ret = String.format("unRegister Client: ip=%s, ID=%s, Total=%d", ip, theID, count);
    System.out.println(ret);
    return ret;
  }

  // get client count
  @WebMethod
  public int getClientCount() {
    int count = AllHosts.getClientCount();
    String ret = String.format("Client Count: %d", count);
    System.out.println(ret);
    return count;
  }

  // Add new request from client
  @WebMethod
  public String AddRequest(int MaxDigit, long ClientID) {
    String reqc = AllReqs.AddNewRequest(MaxDigit, ClientID);

    String ret = "Add New request --- MaxDigit:" + MaxDigit + " ClientID=" + ClientID + " ReqCount=" + reqc;
    System.out.println(ret);
    return ret;
  }

  // get request progress
  @WebMethod
  public float getRequestProgress(long ClientID) {
    float progress = AllReqs.RequestProgress(ClientID);
    String ret = String.format("Get request progress --- ClientID=%d -- Progress=%.2f ", ClientID, progress);
    System.out.println(ret);
    return progress;
  }

  // get request progress
  @WebMethod
  public int getRequestNotDone(long ClientID) {
    int notDone = AllReqs.RequestNotDone(ClientID);
    String ret = String.format("Get request NOT done --- ClientID=%d -- NOTdone=%d ", ClientID, notDone);
    System.out.println(ret);
    return notDone;
  }

  // get last PHI
  @WebMethod
  public String getRequestLastPhi(long ClientID) {
    String phi = AllReqs.getLastPhi(ClientID);
    String ret = String.format("Get Last PHI --- ClientID=%d -- PHI=%s ", ClientID, phi);
    System.out.println(ret);
    return phi;
  }

  // get single job
  @WebMethod
  public int getSingleJob(long SolverID) {
    int ADigit = AllReqs.getSingleFreeJob(SolverID);
    String ret = String.format("Get Single Job --- SolverID=%d --- Digit= ", SolverID, ADigit);
    System.out.println(ret);
    return ADigit;
  }

  // get MULTI job
  @WebMethod
  public String getMultiJob(int JobCount, long SolverID) {
    String jobs = AllReqs.getMultiFreeJobs(JobCount, SolverID);
    String ret = String.format("Get Multi Job --- SolverID=%d --- Digits= %s", SolverID, jobs);
    System.out.println(ret);
    return jobs;
  }

  // set RESULT
  @WebMethod
  public void postJobResult(int ADigit, String Result, long SolverID) {
    // update job using thread
    new JobResultThread(AllReqs, ADigit, Result, SolverID).start();
    
    //AllReqs.setJobResult(ADigit, Result);
    //String ret = "Post Job Result --- Digit= " + String.valueOf(ADigit) + " Result= " + Result;
    //System.out.println(ret);
  }

}

