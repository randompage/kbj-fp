package ws.kbj.middle;

import java.util.ArrayList;
import java.util.List;
import static java.lang.Math.abs;

/**
 */
public class PhiAllRequests {

  private final List<PhiJob> AllJobs;
  private final List<PhiRequest> AllReqs;

  public PhiAllRequests() {
    AllJobs = new ArrayList<PhiJob>();
    AllReqs = new ArrayList<PhiRequest>();
  }

  public String AddNewRequest(int MaxDigit, long ClientID) {
    PhiRequest areq = new PhiRequest(MaxDigit, this, ClientID);
    AllReqs.add(areq);
    int jobc = PopulateJobs(MaxDigit, areq);
    return AllReqs.size() + ":" + jobc;
  }

  public int SearchRequestIdx(long AClientID) {
    int ret = -1;
    long del = -1, aid = -1;
    for (int i = 0, n = AllReqs.size(); i < n; i++) {
      aid = AllReqs.get(i).getClientID();
      del = abs(aid - AClientID);
      if (aid == AClientID || del == 0) {
        ret = i;
        break;
      }
    }
    return ret;
  }

  public float RequestProgress(long AClientID) {
    int Idx = SearchRequestIdx(AClientID);

    if (Idx >= 0 && Idx < AllReqs.size()) {
      PhiRequest areq = AllReqs.get(Idx);

      // check bad done
      int Index = areq.getMaxDigit();
      Check_BadDoneJobs_atIdx(Index);

      // get progress from request
      return areq.getProgress();
    } else {
      return 0;
    }
  }

  public int RequestNotDone(long AClientID) {
    int Idx = SearchRequestIdx(AClientID);

    if (Idx >= 0 && Idx < AllReqs.size()) {
      PhiRequest areq = AllReqs.get(Idx);

      // check bad done
      int Index = areq.getMaxDigit();
      Check_BadDoneJobs_atIdx(Index);

      // get progress from request
      return areq.getNotDone();
    } else {
      return 0;
    }
  }

  public String getLastPhi(long AClientID) {
    int Idx = SearchRequestIdx(AClientID);

    if (Idx >= 0 && Idx < AllReqs.size()) {
      PhiRequest areq = AllReqs.get(Idx);

      // check bad done
      int Index = areq.getMaxDigit();
      Check_BadDoneJobs_atIdx(Index);

      // get LAST PHI
      return areq.getLastPhi();
    } else {
      return "--- ClientID not found ---";
    }
  }

  private void Check_BadDoneJobs_atIdx(int Idx) {
    PhiJob ajob;
    for (int i = 0; i < Idx; i++) {
      ajob = AllJobs.get(i);
      if (ajob.IsBadDone()) {
        ajob.InitJob(); //undone
      }
    }
  }

  private int SearchJobIdx(int ADigit) {
    int ret = -1, adig = -1, del = -1;

    //System.out.println("AllJobs Count==" + AllJobs.size());
    for (int i = 0, n = AllJobs.size(); i < n; i++) {
      adig = AllJobs.get(i).getDigit();
      del = abs(adig - ADigit);
      if (adig == ADigit || del == 0) {
        ret = i;
        //System.out.println("job Found.. " + i);
        break;
      } else if (ADigit == i) {
        //System.out.println("---- job NOT Found.. i=" + i + " digit=" + adig);
      }
    }

    return ret;
  }

  private int PopulateJobs(int MaxDigit, PhiRequest AReq) {
    PhiJob ajob;
    int Idx = -1;

    for (int i = 0; i < MaxDigit; i++) {
      Idx = SearchJobIdx(i);
      if (Idx < 0) {
        ajob = new PhiJob(i);//
        AllJobs.add(ajob);
        AReq.AddJob(ajob);
        //System.out.println("NEW job.. " + i);
      } else if (Idx >= 0 && Idx < AllJobs.size()) {
        AReq.AddJob(AllJobs.get(Idx));
        //System.out.println("old job.. " + i);
      }
    }

    return AllJobs.size();
  }

  public int getSingleFreeJob(long SolverID) {
    int ret = -1;
    PhiJob ajob = null;

    // shuffle
    long seed = System.nanoTime();
    //Collections.shuffle(AllJobs, new Random(seed));

    for (int i = 0, n = AllJobs.size(); i < n; i++) {
      ajob = AllJobs.get(i);
      if (ajob != null) {
        if (ajob.canAllocate()) {
          ajob.Allocate(SolverID);
          ret = i;
          break;
        }
      }
    }

    // check bad done
    Check_BadDoneJobs_atIdx(ret);

    return ret;
  }

  public String getMultiFreeJobs(int JobCount, long SolverID) {
    String[] jobsDigits = new String[JobCount];
    PhiJob ajob = null;
    int dig = -1, digMax = -1;

    // shuffle
    long seed = System.nanoTime();
    //Collections.shuffle(AllJobs, new Random(seed));

    int jobsIdx = 0;
    for (int i = 0, n = AllJobs.size(); i < n; i++) {
      ajob = AllJobs.get(i);
      //if (ajob != null) {
      if (ajob.canAllocate() && (jobsIdx < JobCount)) {
        dig = ajob.getDigit();
        jobsDigits[jobsIdx] = String.valueOf(dig);
        jobsIdx++;
        ajob.Allocate(SolverID);
        //System.out.println("Allocate Job: " + i + " Idx=" + jobsIdx);
        if (dig > digMax) {
          digMax = dig;
        }
      }

      if (jobsIdx >= JobCount) {
        //System.out.println("Allocate Job (BREAK): Idx=" + jobsIdx);
        break;
      }
      //}
    }

    // check bad done
    Check_BadDoneJobs_atIdx(digMax);

    return String.join(",", jobsDigits);
  }

  public void setJobResult(int ADigit, String AResult, long SolverID) {
    int Idx = SearchJobIdx(ADigit);
    if (Idx >= 0 && Idx < AllJobs.size()) {
      PhiJob ajob = AllJobs.get(Idx);
      ajob.JobDone(AResult, SolverID);
      //System.out.println("Job DONE: OK, digit " + ADigit + " OK ");
    } else {
      //System.out.println("Job: NOT FOUND, digit " + ADigit + " NULL ");
    }
  }

}
