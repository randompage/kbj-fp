package ws.kbj.middle;

/**
 *
 * @author root
 */
public class PhiJob {

  private int Digit = 0;
  private boolean HasDone = false;
  private String Result = "";
  private long AllocExpired = 0;
  private long CreateTime = 0;
  private long SolverID_Start = 0;
  private long SolverID_End = 0;

  public PhiJob(int ADigit) {
    InitJob();
    Digit = ADigit;
    CreateTime = System.currentTimeMillis();
  }

  public void InitJob() {
    HasDone = false;
    Result = "";
    AllocExpired = 0;
    SolverID_Start = 0;
    SolverID_End = 0;
  }

  public void JobDone(String AResult, long SolverID) {
    Result = AResult;
    HasDone = true;
    AllocExpired = 0;
    SolverID_End= SolverID;
  }

  public boolean IsDone() {
    return HasDone;
  }

  public boolean IsBadDone() {
    return HasDone && Result.length() < 1;
  }

  public int getDigit() {
    return Digit;
  }

  public String getResult() {
    return Result;
  }

  public boolean canAllocate() {
    long now = System.currentTimeMillis();
    return HasDone ? false : (now < AllocExpired ? false : true);
  }

  public void Allocate(long SolverID) {
    AllocExpired = System.currentTimeMillis() + 10000; // 10 seconds
    SolverID_Start= SolverID;
  }
}
