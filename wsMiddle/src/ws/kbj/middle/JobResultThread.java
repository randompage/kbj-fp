/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.kbj.middle;

/**
 *
 * @author root
 */
public class JobResultThread extends Thread {

  private PhiAllRequests AllRegs;
  private int Digit;
  private String Result;
  private long SolverID;

  public JobResultThread(PhiAllRequests theReqs, int ADigit, String AResult, long ASolverID) {
    AllRegs = theReqs;
    Digit = ADigit;
    Result = AResult;
    SolverID = ASolverID;
  }

  @Override
  public void run() {
    try {
      AllRegs.setJobResult(Digit, Result, SolverID);
    } catch (Exception e) {
      // did nothing
    }
  }
}
