package ws.kbj.middle;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 */
public class PhiRequest {

  private int maxDigit = 0;
  private final List<PhiJob> Jobs;
  private PhiAllRequests AllRequests;
  private long ClientID = 0;

  public PhiRequest(int amaxDigit, PhiAllRequests AllReq, long AClientID) {
    Jobs = new ArrayList<PhiJob>();
    maxDigit = amaxDigit;
    AllRequests = AllReq;
    ClientID = AClientID;
  }

  public void AddJob(PhiJob AJob) {
    Jobs.add(AJob);
  }

  public long getClientID() {
    return ClientID;
  }

  public float getProgress() {
    return (Jobs.size() - getNotDone()) / Jobs.size() * 100;
  }

  public int getNotDone() {
    int NotDone = 0;
    for (int i = 0, n = Jobs.size(); i < n; i++) {
      if (!Jobs.get(i).IsDone()) {
        NotDone++;
      }
    }
    return NotDone;
  }
  
  public int getMaxDigit(){
    return maxDigit;
  }
  
  public String getLastPhi(){
    MathContext mc = new MathContext(maxDigit, RoundingMode.HALF_DOWN);
    
    BigDecimal front = new BigDecimal(BigInteger.ONE, mc).divide(new BigDecimal("2").pow(6));

    /*
    BigDecimal sum = new BigDecimal(BigInteger.ZERO, mc);
    for (int i=0, n=Jobs.size(); i<n; i++){
      sum= sum.add(new BigDecimal(Jobs.get(i).getResult(), mc));
    }
    sum= sum.multiply(front, mc);
    return sum.toPlainString(); //*/
    
    PhiSumThreading sumThread= new PhiSumThreading(Jobs, maxDigit);
    return sumThread.sumAll.multiply(front, mc).toPlainString();
  }

}

