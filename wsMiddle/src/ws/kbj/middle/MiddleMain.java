package ws.kbj.middle;

import java.net.SocketException;
import javax.xml.ws.Endpoint;
import org.dk.ipUtils;

/**
 */
public class MiddleMain {

  private static PhiAllRequests AllReqs;
  private static HostList AllHosts;
  
  private static void OpenWebService_Middle() throws SocketException {
    try {
      String url = "http://0.0.0.0:9191/middle";
      Endpoint.publish(url, new MiddleSvc(AllReqs, AllHosts));
    } catch (Exception e) {
      System.err.println("Caught IOException: " + e.getMessage());
    }

    System.out.println("Middle web service ready at port 9191..");
    
    String ip= ipUtils.getLocalIp();
    System.out.format("Find WSDL at http://%s:9191/middle?WSDL", ip);
  }

  /**
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) throws SocketException {
    AllReqs = new PhiAllRequests();
    AllHosts = new HostList();
    
    OpenWebService_Middle();
  }

}
