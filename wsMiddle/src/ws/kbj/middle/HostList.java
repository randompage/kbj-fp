/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.kbj.middle;

import java.util.HashMap;
import java.util.Map;

/**
 */
public class HostList {
  private Map<String, String> Solvers, Clients;
  
  public HostList(){
    Clients= new HashMap<String, String>();
    Solvers= new HashMap<String, String>();
  }

  
  public int RegisterSolver(String ip, String theID){
    Solvers.put(ip, theID);
    return Solvers.size();
  }
  
  public int unRegisterSolver(String ip, String theID){
    Solvers.remove(ip, theID);
    return Solvers.size();
  }
  
  public int getSolverCount(){
    return Solvers.size();
  }

  
  public int RegisterClient(String ip, String theID){
    Clients.put(ip, theID);
    return Clients.size();
  }
  
  public int unRegisterClient(String ip, String theID){
    Clients.remove(ip, theID);
    return Clients.size();
  }
  
  public int getClientCount(){
    return Clients.size();
  }
  
  
}
