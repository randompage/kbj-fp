package ws.kbj.middle;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class PhiSumThreading {

  private List<PhiJob> Jobs;
  private int maxDigit;
  private MathContext mc;
  private Semaphore allDone;
  public BigDecimal sumAll;

  public PhiSumThreading(List<PhiJob> theJobs, int amaxDigit) {
    Jobs = theJobs;
    maxDigit = amaxDigit;
    mc = new MathContext(maxDigit, RoundingMode.HALF_DOWN);
    
    runSumParallel();
  }
  
  // Worker inner class to add up a section of the array. 
  private class Worker extends Thread {

    int start = -1, end = -1; // jobs index
    BigDecimal sum; // sum container

    Worker(int start, int end) {
      this.start = start;
      this.end = end;
      sum = new BigDecimal(BigInteger.ZERO, mc);
    }

    @Override
    public void run() {
      for (int i = start; i < end; i++) {
        sum= sum.add(new BigDecimal(Jobs.get(i).getResult(), mc));
      }
      allDone.release();
    }

    public BigDecimal getSum() {
      return sum;
    }
  }
  // This is the key method -- launch all the workers, 
  // wait for them to finish. 

  public BigDecimal runSumParallel() {
    // compute workers based on jobs length
    int numWorkers = 10;
    if (Jobs.size() < 10) numWorkers= 1;
    int jobPerWorker= Jobs.size() / numWorkers;
    if (jobPerWorker > 1000) numWorkers= Jobs.size() / 1000;
    if (numWorkers > 50) numWorkers= 50;
    
    allDone = new Semaphore(0);
    
    // Make and start all the workers, keeping them in a list. 
    List<Worker> workers = new ArrayList<Worker>();
    int lenOneWorker = Jobs.size() / numWorkers;
    for (int i = 0; i < numWorkers; i++) {
      int start = i * lenOneWorker;
      int end = (i + 1) * lenOneWorker;
      // Special case: make the last worker take up all the excess. 
      if (i == numWorkers - 1) {
        end = Jobs.size();
      }
      Worker worker = new Worker(start, end);
      workers.add(worker);
      worker.start();
    }
    
    // Wait to finish (this strategy is an alternative to join()) 
    try {
      allDone.acquire(numWorkers);
      // Note: here use acquire(N) .. 
      // could instead init semaphore with -9 and use 
      // regular acquire() here 
    } catch (InterruptedException ignored) {
      //
    }
    
    // Gather sums from workers (yay foreach!) 
    sumAll = new BigDecimal(BigInteger.ZERO);
    for (Worker w : workers) {
      sumAll= sumAll.add(w.getSum(), mc);
    }
    
    return sumAll;
  }
}
