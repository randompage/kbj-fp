/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dk;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.net.util.SubnetUtils;

/**
 *
 * @author root
 */
public class ipUtils {

  public static String getLocalIp() throws SocketException {
    Enumeration<NetworkInterface> enumNI = NetworkInterface.getNetworkInterfaces();

    String ret = "";
    while (enumNI.hasMoreElements()) {
      NetworkInterface ifc = enumNI.nextElement();
      if (ifc.isUp()) {
        Enumeration<InetAddress> enumAdds = ifc.getInetAddresses();
        while (enumAdds.hasMoreElements()) {
          InetAddress addr = enumAdds.nextElement();
          if (!addr.getHostAddress().startsWith("127")) {
            ret = addr.getHostAddress();
            break;
          }
        }
      }
    }

    return ret;
  }
  
  public static String[] getLocalIpAddresses() throws Exception {
    Enumeration<NetworkInterface> enumNI = NetworkInterface.getNetworkInterfaces();
    List<String> list = new ArrayList<String>();

    while (enumNI.hasMoreElements()) {
      NetworkInterface ifc = enumNI.nextElement();
      if (ifc.isUp()) {
        Enumeration<InetAddress> enumAdds = ifc.getInetAddresses();
        while (enumAdds.hasMoreElements()) {
          InetAddress addr = enumAdds.nextElement();
          if (!addr.getHostAddress().startsWith("127")) {
            list.add(addr.getHostAddress());
          }
        }
      }
    }

    // convert to String array
    String[] rets = new String[list.size()];
    rets = list.toArray(rets);

    return rets;
  }

  private static String intToIP(int ipAddress) {
    String ret = String.format("%d.%d.%d.%d", (ipAddress & 0xff),
            (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff),
            (ipAddress >> 24 & 0xff));

    return ret;
  }

  public static InetAddress getIPv4LocalNetMask(InetAddress ip, int netPrefix) {

    try {
      // Since this is for IPv4, it's 32 bits, so set the sign value of
      // the int to "negative"...
      int shiftby = (1 << 31);
      // For the number of bits of the prefix -1 (we already set the sign bit)
      for (int i = netPrefix - 1; i > 0; i--) {
        // Shift the sign right... Java makes the sign bit sticky on a shift...
        // So no need to "set it back up"...
        shiftby = (shiftby >> 1);
      }
      // Transform the resulting value in xxx.xxx.xxx.xxx format, like if
      /// it was a standard address...
      String maskString = Integer.toString((shiftby >> 24) & 255) + "." + Integer.toString((shiftby >> 16) & 255) + "." + Integer.toString((shiftby >> 8) & 255) + "." + Integer.toString(shiftby & 255);
      // Return the address thus created...
      return InetAddress.getByName(maskString);
    } catch (Exception e) {
      e.printStackTrace();
    }
    // Something went wrong here...
    return null;
  }

  public static String[] getIpAddressInSubnet() throws Exception {
    String[] rets = new String[0];
    Enumeration list = NetworkInterface.getNetworkInterfaces();

    while (list.hasMoreElements()) {
      NetworkInterface iface = (NetworkInterface) list.nextElement();

      if (iface == null) {
        continue;
      }

      if (!iface.isLoopback() && iface.isUp()) {

        Iterator it = iface.getInterfaceAddresses().iterator();
        while (it.hasNext()) {
          InterfaceAddress address = (InterfaceAddress) it.next();
          if (address == null) {
            continue;
          }
          InetAddress broadcast = address.getBroadcast();
          InetAddress ipAddress = address.getAddress();
          short prefixLength = address.getNetworkPrefixLength();
          InetAddress subnet = getIPv4LocalNetMask(ipAddress, prefixLength);

          if (broadcast != null && ipAddress != null) {
            //System.out.println(ipAddress.getHostAddress());
            //System.out.println(subnet.getHostAddress());

            SubnetUtils utils = new SubnetUtils(ipAddress.getHostAddress(), subnet.getHostAddress());
            String[] addresses = utils.getInfo().getAllAddresses();
            rets = (String[]) ArrayUtils.addAll(addresses, rets);
          }
        }
      }
    }

    return rets;
  }
}
